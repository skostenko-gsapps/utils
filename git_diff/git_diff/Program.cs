﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace git_diff
{
    class Program
    {
        private static string ExecuteCommand(string command)
        {
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = false;
            processInfo.UseShellExecute = false;

            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            string output = process.StandardOutput.ReadToEnd();
            process.Close();
            return output;
        }

        static void Main(string[] args)
        {
            var default_args = new Dictionary<string, string>();
            default_args.Add("work_dir", Directory.GetCurrentDirectory().ToString());
            default_args.Add("from_branch", "master");
            default_args.Add("to_branch", "development");

            var user_args = new Dictionary<string, string>();
            foreach (string argument in args)
            {
                string[] splitted = argument.Split('=');
                if (splitted.Length == 2)
                {
                    user_args[splitted[0]] = splitted[1];
                }
            }

            Dictionary<string, string> ARGS = new Dictionary<string, string>(default_args);
            foreach (KeyValuePair<String, String> kvp in user_args)
            {
                ARGS[kvp.Key] = kvp.Value;
            }

            string command = string.Format("cd /D {0} && git diff --name-status --oneline {1}...{2}", ARGS["work_dir"], ARGS["from_branch"], ARGS["to_branch"]);
            string file_names = ExecuteCommand(command);
 
            if (!string.IsNullOrEmpty(file_names))
            {
                string changed_dir = string.Format(@"{0}\changed_procs_{1}", ARGS["work_dir"], DateTime.Now.ToString("MM_dd_yyyy__HH_mm"));
                Directory.CreateDirectory(changed_dir);
    
                var changed_list_file = new StreamWriter(@changed_dir + "\\changed.txt");
                var removed_list_file = new StreamWriter(@changed_dir + "\\removed.txt");

                char[] split_char = new char[] {'\n'};
                string[] fn = file_names.Split(split_char, StringSplitOptions.RemoveEmptyEntries);

                foreach (string line in fn)
                {
                    char flag = line[0];
                    string file_name = (line.Split('\t')[1]).Replace('/', '\\');
                    string file_ext = Path.GetExtension(file_name);
                    // Console.WriteLine("Flag:" + flag + " Name:" + file_name + " Ext:" + file_ext);

                    if (file_ext.Equals(".sql")) {
                        if (flag.Equals('D')) {
                            removed_list_file.WriteLine(file_name);
                        }
                        else
                        {
                            changed_list_file.WriteLine(file_name);
                            string sourceFile = Path.Combine(ARGS["work_dir"], @file_name.ToString());
                            string destFile = Path.Combine(changed_dir, @file_name.ToString());

                            FileInfo fi = new FileInfo(destFile);
                            if (!fi.Directory.Exists)
                            {
                                Directory.CreateDirectory(fi.DirectoryName);
                            }

                            File.Copy(sourceFile, destFile);
                        }
                    }
                }
                changed_list_file.Close();
                removed_list_file.Close();
            }
        }
    }
}
