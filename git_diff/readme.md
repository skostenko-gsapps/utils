# Git diff RMO's stored procedures
Usage:
```
git_diff
```
will create dir with name ```changed_procs_<current_date>``` that will contain:

- changed.txt -- text file with list of changed stored procedures (.sql)

- deleted.txt -- text file with list of changed stored procedures (.sql)

- directory tree with changed files

Optional parameters:

- ```work_dir``` can be used if you want to run script outside of dir, that contains RMO database repository

   Example:
   ```
   git_diff work_dir=D:\git_repos\RMO_DB
   ```
   
- ```from_branch``` (default: 'master') and ```to_branch``` (default: 'development') can be used to set branches that should be compared.

  Example:
  ```
  git_diff from_brach=test to_branch=development
  ```